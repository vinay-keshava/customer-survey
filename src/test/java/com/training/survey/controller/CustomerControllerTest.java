package com.training.survey.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.survey.dto.CustomerDto;
import com.training.survey.dto.ResponseDto;
import com.training.survey.service.CustomerService;

@ExtendWith(SpringExtension.class)
class CustomerControllerTest {
	
	@Mock
	CustomerService customerService;
	
	@InjectMocks
	CustomerController customerController;

	@Test
	void testSucess() {
		CustomerDto customerDto = new CustomerDto("CHAITRA","SHETTY","FEMALE","MIJAR");
		Mockito.when(customerService.addCustomer(any(CustomerDto.class))).thenReturn(new ResponseDto( "Customer Added successfully",200));
		ResponseEntity<ResponseDto> response = customerController.addCustomer(customerDto);
		assertEquals(HttpStatusCode.valueOf(201), response.getStatusCode());
	}


}
