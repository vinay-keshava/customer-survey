package com.training.survey.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.training.survey.dto.CoordinateDto;
import com.training.survey.dto.SurveyDto;
import com.training.survey.dto.SurveyResponseDto;
import com.training.survey.entity.Branch;
import com.training.survey.entity.Customer;
import com.training.survey.entity.Gender;
import com.training.survey.exception.BranchNotFoundException;
import com.training.survey.exception.CustomersNotFoundWithinLimit;
import com.training.survey.repository.BranchRepository;
import com.training.survey.repository.CustomerRepository;
import com.training.survey.repository.SurveyRepository;

	

@ExtendWith(MockitoExtension.class)
class SurveyServiceImplTest {
	 
		@InjectMocks
		SurveyServiceImpl serviceImpl;
	 
		@Mock
		CoordinateService coordinateService;
	 
		@Mock
		BranchRepository branchRepository;
	 
		@Mock
		CustomerRepository customerRepository;
	 
		@Mock
		SurveyRepository surveyRepository;
	 
		@Test
		void testBranchNotFound() {
	 
			SurveyDto surveyDto = new SurveyDto();
			surveyDto.setBranchId(1L);
			surveyDto.setDistance(100.00);
	 
			Branch branch = new Branch();
			branch.setBranchId(4L);
			branch.setBranchAddress("bengaluru");
			branch.setBranchName("SBI");
	 
			Mockito.when(branchRepository.findByBranchId(surveyDto.getBranchId())).thenReturn(null);
			assertThrows(BranchNotFoundException.class, () -> {
				serviceImpl.surveyDetails(surveyDto);
			});
			
		}
	 
		@Test
		void testMethodSurvey() {
	 
			Branch branch = new Branch();
			branch.setBranchId(4L);
			branch.setBranchAddress("bengaluru");
			branch.setBranchName("SBI");
	 
			Mockito.when(branchRepository.findByBranchId(Mockito.anyLong())).thenReturn(branch);
	 
			CoordinateDto coordinateDto = new CoordinateDto();
			coordinateDto.setLatitude(12.00);
			coordinateDto.setLongitude(44.00);
	 
			Mockito.when(coordinateService.getCoordinate(Mockito.anyString())).thenReturn(coordinateDto);
	 
			Customer customer = new Customer();
			customer.setAddress("Bengaluru");
			customer.setCustomerId(4L);
			customer.setFirstName("Maheesh");
			customer.setGender(Gender.MALE);
	 
			List<Customer> customerList = List.of(customer);
	 
			Mockito.when(customerRepository.findAll()).thenReturn(customerList);
	 
			coordinateDto.setLatitude(13.00);
			coordinateDto.setLongitude(47.00);
	 
			SurveyDto dto = new SurveyDto();
			dto.setBranchId(2l);
			dto.setDistance(10d);
			SurveyResponseDto request = serviceImpl.surveyDetails(dto);
			assertNotNull(request);
	 
		}
	 
		@Test
		void testMethodSurveyMoreDistance() {
	 
			Branch branch = new Branch();
			branch.setBranchId(4L);
			branch.setBranchAddress("bengaluru");
			branch.setBranchName("SBI");
	 
			Mockito.when(branchRepository.findByBranchId(Mockito.anyLong())).thenReturn(branch);
	 
			CoordinateDto coordinateDto = new CoordinateDto();
			coordinateDto.setLatitude(-66.673289);
			coordinateDto.setLongitude(-154.50103);
	 
			Mockito.when(coordinateService.getCoordinate(Mockito.anyString())).thenReturn(coordinateDto);
	 
			Customer customer = new Customer();
			customer.setAddress("Bengaluru");
			customer.setCustomerId(4L);
			customer.setFirstName("Maheesh");
			customer.setGender(Gender.MALE);
	 
			List<Customer> customerList = List.of(customer);
	 
			Mockito.when(customerRepository.findAll()).thenReturn(customerList);
	 
			coordinateDto.setLatitude(-89.319112);
			coordinateDto.setLongitude(130.335399);
	 
			SurveyDto dto = new SurveyDto();
			dto.setBranchId(2l);
			dto.setDistance(0d);
			SurveyResponseDto request = serviceImpl.surveyDetails(dto);
			assertNotNull(request);
	 
		}
	 
		@Test
		void testDistance_WhenLocationIsSame() {
	 
			double lat1 = -89.319112;
			double lat2 = -89.319112;
	 
			double long1 = 130.335399;
			double long2 = 130.335399;
	 
			assertEquals(0, SurveyServiceImpl.distance(lat1, long1, lat2, long2, "K"));
	 
		}
		
		@Test
		void testDistance_WhenLocationIs_NotSame() {
	 
			double lat1 = -89.319112;
			double lat2 = -66.673289;
	 
			double long1 = 130.335399;
			double long2 = -154.50103;
	 

			assertNotEquals(0, SurveyServiceImpl.distance(lat1, long1, lat2, long2, "K"));
	 
		}
		
		@Test
		void testDistance_WhenLocationIs_NotSame_UnitIsN() {
	 
			double lat1 = -89.319112;
			double lat2 = -66.673289;
	 
			double long1 = 130.335399;
			double long2 = -154.50103;
	 
			assertNotEquals(0, SurveyServiceImpl.distance(lat1, long1, lat2, long2, "N"));
	 
		}
		
		
	 
	}

