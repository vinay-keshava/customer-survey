package com.training.survey.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.survey.entity.Customer;

import jakarta.validation.constraints.NotBlank;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

	Optional<Customer> findByFirstName(@NotBlank(message = "customer first name is mandatory") String firstName);

}
