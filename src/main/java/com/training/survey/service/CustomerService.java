package com.training.survey.service;

import com.training.survey.dto.CustomerDto;
import com.training.survey.dto.ResponseDto;

import jakarta.validation.Valid;

public interface CustomerService {

	ResponseDto addCustomer(@Valid CustomerDto customerDto);

}
