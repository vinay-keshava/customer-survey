package com.training.survey.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.training.survey.dto.SurveyExportDto;
import com.training.survey.entity.Survey;
import com.training.survey.repository.SurveyRepository;
import com.training.survey.service.SurveyExportService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class SurveyExportServiceImpl implements SurveyExportService {

	private SurveyRepository surveyRepository;

	@Override
	public List<SurveyExportDto> exportSurveyDetails() {

		List<Survey> surveyDetails = surveyRepository.findAll();
		log.info("retrived all the record of Survey");
		List<SurveyExportDto> dtos = new ArrayList<>();

		for (Survey surveys : surveyDetails) {
			log.info("mapping each survey entity to surveyExportDto ");
			SurveyExportDto exportDto = new SurveyExportDto();
			exportDto.setBranchId(surveys.getBranch().getBranchId());
			exportDto.setCustomerName(surveys.getCustomer().getFirstName());
			exportDto.setAddress(surveys.getAddress());
			exportDto.setDistance(surveys.getDistance());
			dtos.add(exportDto);

		}
		log.info("csv exported");
		return dtos;
		

	}

}
