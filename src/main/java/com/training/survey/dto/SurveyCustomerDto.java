package com.training.survey.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter 
public class SurveyCustomerDto {

	String customerName;
	String customerAddress;
	Double distance;
}
