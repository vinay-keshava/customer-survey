package com.training.survey.dto;

public record ResponseDto(String message, int httpStatusCode) {

}
