package com.training.survey.dto;

import jakarta.validation.constraints.NotBlank;

public record CustomerDto(
		@NotBlank(message = "customer first name is mandatory") String firstName,
		@NotBlank(message = "customer last name is mandatory") String lastName,
		@NotBlank(message = "Gender is mandatory") String gender,
		@NotBlank(message = "Address is mandatory") String address
		) {

	

}
