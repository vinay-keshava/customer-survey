package com.training.survey.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SurveyResponseDto {
	
			Long branchId;
			List<SurveyCustomerDto> customerDtos;
		
}
