package com.training.survey.dto;

import lombok.Data;

@Data
public class SurveyExportDto {

	
	private Long branchId;
	private String customerName;
	private String address;
	private Double distance;
}
